﻿Shader "Hidden/Ripple4Shader"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_RefTex("Reference Texture", 2D) = "white" {}
		_StrengthTex("Strength Texture", 2D) = "white" {}
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			sampler2D _RefTex;
			sampler2D _StrengthTex;

			fixed4 frag (v2f i) : SV_Target
			{
				float2 uvPos = float2(i.uv.x, i.uv.y);
				fixed4 refCol = tex2D(_RefTex, uvPos);
				fixed4 strCol = tex2D(_StrengthTex, uvPos);
				float2 refPos = float2(refCol.r - 0.5, refCol.g - 0.5);
				//float2 grabPos = float2(uvPos.x + (refPos.x - uvPos.x), uvPos.y + (refPos.y - uvPos.y));
				float strength = strCol.r * 10;
				float2 grabPos = float2(uvPos.x + (refPos.x / (128.0 / strength)), uvPos.y + (refPos.y / (80.0 / strength)));
				//float2 grabPos = float2(refPos.x / (128.0 / 2), refPos.y / (80.0 / 2));

				fixed4 col = tex2D(_MainTex, grabPos);
				//float reflection = (tex2D(_RefTex, float2(uvPos.x - 0.01, uvPos.y - 0.01)).a + tex2D(_RefTex, float2(uvPos.x + 0.01, uvPos.y + 0.01)).a + tex2D(_RefTex, float2(uvPos.x + 0.01, uvPos.y - 0.01)).a + tex2D(_RefTex, float2(uvPos.x - 0.01, uvPos.y + 0.01)).a) / 4;
				col = col + refCol.a;
				//col = col + (tex2D(_ReflectTex, grabPos) * (1 - refCol.a));
				//float refraction = (tex2D(_RefTex, float2(uvPos.x - 0.01, uvPos.y - 0.01)).b + tex2D(_RefTex, float2(uvPos.x + 0.01, uvPos.y + 0.01)).b + tex2D(_RefTex, float2(uvPos.x + 0.01, uvPos.y - 0.01)).b + tex2D(_RefTex, float2(uvPos.x - 0.01, uvPos.y + 0.01)).b) / 4;
				col = col - refCol.b;
				//col = col + (tex2D(_RefractTex, grabPos) * (1 - refCol.b));
				//col.a = refCol.a;

				return col;
			}
			ENDCG
		}
	}
}
