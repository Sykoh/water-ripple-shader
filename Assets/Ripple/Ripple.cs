﻿using UnityEngine;
using System.Collections;

public class Ripple : MonoBehaviour {

    public int _scale = 0;
    
    int _waveWidth;
    int _waveHeight;
    int _activeBuffer = 0;
    int _bmpHeight, _bmpWidth;

    public int width = 128;
    public int height = 128;

    int[] _waves1;
    int[] _waves2;

    Color[] pixelOffsets;
    Texture2D tex;

    public Material material;

    Texture2D strengthTex;
    Color32[] strengthColors;

    public int displacement = 32;
    private float distance = 0.3f;
    private int speed = 1;
    private float reflection = 1;
    private float refraction = 1;
    private float worldScreenHeight;
    private float worldScreenWidth;

    // Pubs
    public int waveStrength = 5;
    public int waveDistance = 2;
    public bool ambientWaves = false;
    public int ambientWaveStrength = 1;


    // Global Instance
    public static Ripple Instance;

    // Use this for initialization
    void Start() {
        Instance = this;

        worldScreenHeight = Camera.main.orthographicSize * 2.0f;
        worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

        // Setup
        reflection = waveStrength / 10f;
        refraction = waveStrength / 10f;
        distance = 1 - (waveDistance / 10f);

        _waveWidth = width << _scale;
        _waveHeight = height << _scale;

        _waves1 = new int[_waveWidth * _waveHeight];
        _waves2 = new int[_waveWidth * _waveHeight];

        pixelOffsets = new Color[_waveWidth * _waveHeight];
        tex = new Texture2D(_waveWidth, _waveHeight);
        tex.wrapMode = TextureWrapMode.Clamp;
        material.SetTexture("_RefTex", tex);

        strengthTex = new Texture2D(_waveWidth, _waveHeight);
        strengthColors = new Color32[_waveWidth * _waveHeight];
        tex.wrapMode = TextureWrapMode.Clamp;
        material.SetTexture("_StrengthTex", strengthTex);

        NoiseSetup();
    }

    void Update(){

        for (int i = 0; i < speed; i++) {
            ProcessWaves();
        }
        
        UpdateFrame();
        if(ambientWaves)
            DistubAmbient();
        Disturb();
    }

    void OnRenderImage(RenderTexture src, RenderTexture dst)
    {
        Graphics.Blit(src, dst, material);
    }

    void ProcessWaves()
    {
        int newBuffer = (_activeBuffer == 0) ? 1 : 0;
        _activeBuffer = newBuffer;

        // Process waves

        for (int y = 0; y < _waveHeight; y++) {
            for (int x = 0; x < _waveWidth; x++) {
                if (x >= (_waveWidth - 1) || y >= (_waveHeight - 1) || x <= 0 || y <= 0)
                {
                    continue;
                }

                if (_activeBuffer == 0)
                {
                    _waves1[(y * _waveWidth) + x] = (int)(((_waves2[((y - 1) * _waveWidth) + (x - 1)] + _waves2[((y - 1) * _waveWidth) + x] + _waves2[((y - 1) * _waveWidth) + (x + 1)] + _waves2[(y * _waveWidth) + (x - 1)] + _waves2[(y * _waveWidth) + (x + 1)] + _waves2[((y + 1) * _waveWidth) + (x - 1)] + _waves2[((y + 1) * _waveWidth) + x] + _waves2[((y + 1) * _waveWidth) + (x + 1)]) >> 2) - _waves1[(y * _waveWidth) + x]);

                    //damping
                    _waves1[(y * _waveWidth) + x] -= (int)(_waves1[(y * _waveWidth) + x] * distance);

                    if (ambientWaves && _waves1[(y * _waveWidth) + x] == 0)
                        _waves1[(y * _waveWidth) + x] += (int)(Random.value * (ambientWaveStrength * 100));
                }
                else if (_activeBuffer == 1)
                {
                    _waves2[(y * _waveWidth) + x] = (int)(((_waves1[((y - 1) * _waveWidth) + (x - 1)] + _waves1[((y - 1) * _waveWidth) + x] + _waves1[((y - 1) * _waveWidth) + (x + 1)] + _waves1[(y * _waveWidth) + (x - 1)] + _waves1[(y * _waveWidth) + (x + 1)] + _waves1[((y + 1) * _waveWidth) + (x - 1)] + _waves1[((y + 1) * _waveWidth) + x] + _waves1[((y + 1) * _waveWidth) + (x + 1)]) >> 2) - _waves2[(y * _waveWidth) + x]);

                    //damping
                    _waves2[(y * _waveWidth) + x] -= (int)(_waves2[(y * _waveWidth) + x] * distance);

                    if (ambientWaves && _waves1[(y * _waveWidth) + x] == 0)
                        _waves2[(y * _waveWidth) + x] += (int)(Random.value * (ambientWaveStrength * 100));
                }
            }
        }
    }

    void UpdateFrame() {

        for (int y = 0; y < _waveHeight; y++)
        {
            for (int x = 0; x < _waveWidth; x++)
            {
                int xOffset;
                int yOffset;
                float alpha;
                float balpha;

                int waveX = (int)x >> _scale;
                int waveY = (int)y >> _scale;

                //check bounds
                if (waveX <= 0) waveX = 1;
                if (waveY <= 0) waveY = 1;
                if (waveX >= _waveWidth - 1) waveX = _waveWidth - 2;
                if (waveY >= _waveHeight - 1) waveY = _waveHeight - 2;

                //this gives us the effect of water breaking the light
                if (_activeBuffer == 0)
                {
                    xOffset = (int)((_waves1[(waveY * _waveWidth) + (waveX - 1)] - (_waves1[(waveY * _waveWidth) + (waveX + 1)] * 0.99f)));
                    yOffset = (int)((_waves1[((waveY - 1) * _waveWidth) + waveX] - (_waves1[((waveY + 1) * _waveWidth) + waveX] * 0.99f)));
                }
                else if (_activeBuffer == 1)
                {
                    xOffset = (int)((_waves2[(waveY * _waveWidth) + (waveX - 1)] - (_waves2[(waveY * _waveWidth) + (waveX + 1)] * 0.99f)));
                    yOffset = (int)((_waves2[((waveY - 1) * _waveWidth) + waveX] - (_waves2[((waveY + 1) * _waveWidth) + waveX] * 0.99f)));
                }
                else {
                    xOffset = 0;
                    yOffset = 0;
                }

                if (xOffset != 0 || yOffset != 0)
                {
                    //check bounds
                    if (x + xOffset >= _waveWidth - 1)
                    {
                        xOffset = _waveWidth - x - 1;
                    }
                    if (y + yOffset >= _waveHeight - 1)
                    {
                        yOffset = _waveHeight - y - 1;
                    }
                    if (x + xOffset < 0)
                    {
                        xOffset = -x;
                    }
                    if (y + yOffset < 0)
                    {
                        yOffset = -y;
                    }

                    //generate alpha
                    int reflect = 0;
                    reflect += xOffset < 0 ? -xOffset : 0;
                    reflect += yOffset < 0 ? -yOffset : 0;
                    reflect = (int)(reflect * Random.Range(0.9f, 1.1f));
                    alpha = 255 - (reflect * reflection);

                    int refract = 0;
                    refract += xOffset > 0 ? xOffset : 0;
                    refract += yOffset > 0 ? yOffset : 0;
                    refract = (int)(refract * Random.Range(0.9f, 1.1f));
                    balpha = 255 - (refract * refraction);

                    //alpha = (float)(255 - (Mathf.Sqrt((xOffset * xOffset) + (yOffset * yOffset)) * refraction));
                    if (alpha < 0) alpha = 0;
                    if (alpha > 255) alpha = 255;
                    if (balpha < 0) balpha = 0;
                    if (balpha > 255) balpha = 255;

                    int index = (y * _waveWidth) + x;
                    Vector2 offset = new Vector2(xOffset, yOffset);
                    pixelOffsets[index].r = (offset.x / _waveWidth) + 0.5f;
                    pixelOffsets[index].g = (offset.y / _waveHeight) + 0.5f;
                    pixelOffsets[index].b = balpha / 255f;
                    pixelOffsets[index].a = alpha / 255f;
                    if (_activeBuffer == 0)
                    {
                        if (_waves1[index] > 1000)
                            strengthColors[index].r = 100;
                        else
                            strengthColors[index].r = 25;
                    }
                    if (_activeBuffer == 1)
                    {
                        if (_waves2[index] > 1000)
                            strengthColors[index].r = 100;
                        else
                            strengthColors[index].r = 25;
                    }
                }
                else {
                    int index = (y * _waveWidth) + x;
                    Vector2 offset = new Vector2(xOffset, yOffset);
                    pixelOffsets[index].r = (offset.x / _waveWidth) + 0.5f;
                    pixelOffsets[index].g = (offset.y / _waveHeight) + 0.5f;
                    pixelOffsets[index].b = 1;
                    pixelOffsets[index].a = 1;
                    if (_activeBuffer == 0)
                    {
                        if (_waves1[index] > 1000)
                            strengthColors[index].r = 50;
                        else
                            strengthColors[index].r = 25;
                    }
                    if (_activeBuffer == 1)
                    {
                        if (_waves2[index] > 1000)
                            strengthColors[index].r = 50;
                        else
                            strengthColors[index].r = 25;
                    }
                }
            }
        }

        for (int y = 1; y < _waveHeight -  1; y++)
        {
            for (int x = 1; x < _waveWidth - 1; x++)
            {
                int index = (y * _waveWidth) + x;
                pixelOffsets[index].r = (pixelOffsets[(y * _waveWidth) + (x + 1)].r + pixelOffsets[(y * _waveWidth) + (x - 1)].r + pixelOffsets[index].r) / 3;
                pixelOffsets[index].g = (pixelOffsets[((y + 1) * _waveWidth) + x].g + pixelOffsets[((y - 1) * _waveWidth) + x].g + pixelOffsets[index].g) / 3;
                pixelOffsets[index].b = (pixelOffsets[((y + 1) * _waveWidth) + x].b + pixelOffsets[(y * _waveWidth) + (x + 1)].b + pixelOffsets[index].b) / 3;
                pixelOffsets[index].a = (pixelOffsets[((y - 1) * _waveWidth) + x].a + pixelOffsets[(y * _waveWidth) + (x - 1)].a + pixelOffsets[index].a) / 3;
            }
        }

        strengthTex.SetPixels32(strengthColors);
        strengthTex.Apply();
        tex.SetPixels(pixelOffsets);
        tex.Apply();
    }

    void Disturb() {
        if (Application.platform == RuntimePlatform.Android) {
            if (Input.touchCount > 0)
            {
                foreach (Touch t in Input.touches) {
                    int normPosX = (int)(t.position.x / Screen.width * width);
                    int normPosY = (int)(t.position.y / Screen.height * height);

                    for (int y = normPosY - 2; y < normPosY + 3; y++)
                    {
                        for (int x = normPosX - 2; x < normPosX + 3; x++)
                        {
                            int index = (y * width) + x;

                            if (_activeBuffer == 0 && _waves1[index] >= -(displacement * 10) && _waves1[index] <= displacement * 10)
                            {
                                _waves1[(y * _waveWidth) + x] += displacement;
                            }
                            if (_activeBuffer == 1 && _waves2[index] >= -(displacement * 10) && _waves2[index] <= displacement * 10)
                            {
                                _waves2[(y * _waveWidth) + x] += displacement;
                            }
                        }
                    }
                }
            }
        }
        else{
            if (Input.GetMouseButton(0))
            {
                int normPosX = (int)(Input.mousePosition.x / Screen.width * width);
                int normPosY = (int)(Input.mousePosition.y / Screen.height * height);

                normPosY = height - normPosY;

                for (int y = normPosY - 2; y < normPosY + 3; y++)
                {
                    for (int x = normPosX - 2; x < normPosX + 3; x++)
                    {
                        int index = (y * width) + x;

                        if (_activeBuffer == 0 && _waves1[index] >= -(displacement * 10) && _waves1[index] <= displacement * 10)
                        {
                            _waves1[(y * _waveWidth) + x] += displacement;
                        }
                        if (_activeBuffer == 1 && _waves2[index] >= -(displacement * 10) && _waves2[index] <= displacement * 10)
                        {
                            _waves2[(y * _waveWidth) + x] += displacement;
                        }
                    }
                }
            }
        }

    }

    float noiseOffset = 0.0f;
    float noiseOffsetDirection = 0.1f;
    float[] noiseHeights;

    void NoiseSetup() {
        noiseHeights = new float[_waveWidth * _waveHeight];
        for (int y = 0; y < _waveHeight; y++)
        {
            for (int x = 0; x < _waveWidth; x++)
            {
                noiseHeights[(y * _waveWidth) + x] = (int)(Random.value * 180);
            }
        }

        for (int i = 0; i < 1; i++)
        {
            for (int y = 0; y < _waveHeight - 1; y++)
            {
                for (int x = 0; x < _waveWidth - 1; x++)
                {
                    noiseHeights[(y * _waveWidth) + x] = (noiseHeights[(y * _waveWidth) + x] + noiseHeights[(y * _waveWidth) + (x + 1)] + noiseHeights[((y + 1) * _waveWidth) + x] + noiseHeights[((y + 1) * _waveWidth) + (x + 1)]) / 4;
                }
            }
        }
    }

    void DistubAmbient() {
        for (int i = 0; i < (_waveWidth * _waveHeight) - 1; i++)
        {
            noiseHeights[i] = (noiseHeights[i] * 0.9f) + (noiseHeights[i + 1] * 0.1f);
            noiseHeights[i] += Time.deltaTime * Random.Range(4, 16) * ambientWaveStrength;
            
            if (noiseHeights[i] >= 180)
                noiseHeights[i] = 0;    
        }
        noiseHeights[(_waveWidth * _waveHeight) - 1] = (int)(Random.value * 180);

        for (int y = 2; y < _waveHeight - 2; y++)
        {
            for (int x = 2; x < _waveWidth - 2; x++)
            {
                int index = (y * _waveWidth) + x;
                float sample = Mathf.Sin(noiseHeights[index] * Mathf.Deg2Rad);
                

                if (_activeBuffer == 0 && _waves1[index] >= -(displacement * 10) && _waves1[index] <= displacement * 10)
                {
                    _waves1[(y * _waveWidth) + x] += (int)(sample * 100);
                }
                if (_activeBuffer == 1 && _waves2[index] >= -(displacement * 10) && _waves2[index] <= displacement * 10)
                {
                    _waves2[(y * _waveWidth) + x] += (int)(sample * 100);
                }
            }
        }
    }

    public void DisturbAtPoint(int x, int y)
    {
        if (x >= 127 || y >= 79 || x <= 0 || y <= 0)
            return;

        for (int yy = y - 1; yy < y + 1; yy++)
        {
            for (int xx = x - 1; xx < x + 1; xx++)
            {
                int index = (yy * _waveWidth) + xx;
                if (_activeBuffer == 0 && _waves1[index] >= -(displacement * 10) && _waves1[index] <= displacement * 10)
                {
                    _waves1[(y * _waveWidth) + x] += 500;
                }
                if (_activeBuffer == 1 && _waves2[index] >= -(displacement * 10) && _waves2[index] <= displacement * 10)
                {
                    _waves2[(y * _waveWidth) + x] += 500;
                }
            }
        }
    }
}
