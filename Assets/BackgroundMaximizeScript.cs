﻿using UnityEngine;
using System.Collections;

public class BackgroundMaximizeScript : MonoBehaviour {

    public Texture2D texture;

    // Use this for initialization
    void Start () {
        float pwidth = GetComponent<Renderer>().bounds.size.x;
        float pheight = GetComponent<Renderer>().bounds.size.y;

        float worldScreenHeight = Camera.main.orthographicSize * 2.0f;
        float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;
        GetComponent<Transform>().localScale = new Vector3(worldScreenWidth / pwidth, worldScreenHeight / pheight, 1);
    }

}
